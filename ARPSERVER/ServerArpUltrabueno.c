/* 
 * File:   ServerArp.c
 * Author: eos
 *
 * Created on April 4, 2013, 4:50 PM
 */
//******************************************************************************
#include <sys/time.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>         
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netpacket/packet.h>
#include <net/ethernet.h>
#include <net/if_arp.h>
#include <netinet/in.h> 
#include <sys/time.h>
#include "Elemento.h"
#include "Lista.h"
#include <unistd.h>
//******************************************************************************
/*
 * 
 */
unsigned char mimac[6];//mi mac 
unsigned char mibroad[8];// direccion de broadcast
unsigned char mimascara[6];//mi mascara de subred
unsigned char miip[4];//mi propia ip
unsigned char MacIpCero[6]={0x00,0x00,0x00,0x00,0x00,0x00};
unsigned char MACbroadcast[6]={0xff,0xff,0xff,0xff,0xff,0xff};//mac destino
unsigned char MACTRON[6]={0x00,0x00,0x5E,0x00,0x01,0x3D};//la mac usada para defender 
unsigned char MacDestino[6];//mac destino 
unsigned char IPDestino[]={0x0A,0xD0,0xDD,0x80};//ip destino  
unsigned char tramaEnv[100];
unsigned char recivTrama[200];
unsigned char ethertype[]={0x0c,0x0c};
//algunos protocolos
unsigned char arp[]={0x08,0x06};//protocolo arp
unsigned char ether[]={0x00,0x01};//protocolo ethernet
unsigned char proip[]={0x08,0x00};//protocolo ip
unsigned char arprequest[]={0x00,0x01};//protocolo de pregunta
unsigned char arpRespuesta[]={0x00,0x02};//protocolo de respuesta 
/******************************************************************************/
void estructuraTramaScanner( unsigned char *trama);
void EscaneaRed(int ds,int index,unsigned char *trama,unsigned char *tramarecivida,lista *l);
int obtenerDatos(int ds);
void TRON(int ds,int index,lista *l);
void EstructuraTramaGratuitaRespuesta(unsigned char *trama,unsigned char *IpInfractor);
void EstructuraTramaGratuitaDefensora(unsigned char *trama,unsigned char *MacInfractor,unsigned char *IpInfractor);
void AgregaBasePaquete(lista *l,elementolista e);
void AgregaBase(lista *l,unsigned char *ip,unsigned char *mac);
int BuscadorBase(lista *l,unsigned char *ip,unsigned char *      mac);
/******************************************************************************/
void AgregarAsuario(void);
void BorrarUsuario(void);
void BuscarUsuario(void);
void Menu(void);
void UsoMenu(void);
void MostrarBase(lista *l);
int cargarDatos(void);
/******************************************************************************/
void TRON(int ds,int index,lista *l)
{
    int tam,indice=0,i,j;
    struct sockaddr_ll nic;    
    struct timeval inicial,final;
    elementolista temporal;
    unsigned char trama[100];//trama a ser estructurada para respuesta de arp
    unsigned char macinfractor[6],ipinfractor[4];//los datos del infractor
    unsigned char TramaAEscuchar[200];
    
    
    //struct timezone base;
    memset(&nic,0x00,sizeof(nic));
    nic.sll_family=AF_PACKET;
    nic.sll_protocol=htons(ETH_P_ARP);
    nic.sll_ifindex=index;
    puts("******************************Servidor Activado******************************");
    while(1)
    {
    //escuchar las tramas entrantes de ARP
        
    tam=recvfrom(ds,TramaAEscuchar,100,MSG_DONTWAIT,NULL,0);//recibe la trama a escuchar
    if(tam==-1)//si recive -1 el socket no tiene ningun mensaje
    {
        //no hay mensaje
    }
    else
    {
    if((memcmp(TramaAEscuchar+28,MacIpCero,4)==0)||(memcmp(TramaAEscuchar+32,MacIpCero,6)==0)) // la trama que recibe es la direccion ip / se checa que corresponda la ip del quien la envia con la ip del quien pregunte
    {
        if(BuscadorBase(l,TramaAEscuchar+0,TramaAEscuchar+6)==False)//busca en la base de datos la coincidencia de los datos 
        {
            //printf("***indice  %d***\n",indice);
            //puts("****Adquiriendo datos del intruso****");
            memcpy(ipinfractor,TramaAEscuchar+38,4);//adquiere los datos
            memcpy(macinfractor,TramaAEscuchar+22,6);
            EstructuraTramaGratuitaDefensora(trama,macinfractor,ipinfractor);
            //puts("****Generando respuestas****");
            tam=sendto(ds,trama,42,0,(struct sockaddr *)&nic,sizeof(nic));//Envia la trama de respuesta
            if(tam ==-1)
            {
                perror("\nerror al enviar la trama ");
            }
            else//es por si si la envia 
            {
                EstructuraTramaGratuitaRespuesta(trama,ipinfractor);
                tam=sendto(ds,trama,42,0,(struct sockaddr *)&nic,sizeof(nic));//actualiza a todos quien es dueño de la ip
                if(tam ==-1)
                {
                    perror("\nerror al enviar la trama ");
                }
                else
                {
                    EstructuraTramaGratuitaRespuesta(trama,ipinfractor);
                    tam=sendto(ds,trama,42,0,(struct sockaddr *)&nic,sizeof(nic));//actualiza a todos quien es dueño de la ip
                    if(tam ==-1)
                    {
                        perror("\nerror al enviar la trama ");
                    }
                    else
                    {
                        puts("***********Intruso bloqueado***********");
                        printf("\nLa direccion Mac: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n",TramaAEscuchar[6],TramaAEscuchar[7],TramaAEscuchar[8],TramaAEscuchar[9],TramaAEscuchar[10],TramaAEscuchar[11]);
                        printf("*******************************************************************************\n");
                    }
                }
            }
            indice++;
        }
        else
        {
            puts("***********Usuario Aceptado***********");            
        }
    }
    else
    {
        //respuesta
    }
    }
    //printf(".");
    }
    
} 
void AgregaBasePaquete(lista *l,elementolista e)//adquiere la ip y la mac agregandole informacion del usuario 
{
    InsertarFinal(l,e);
}
void AgregaBaseExternos(lista *l,unsigned char *ip,unsigned char *mac)//solo adquiere la ip y la mac y la agrega a la base 
{
    elementolista temporal;
    //memcpy(temporal.alias,"No Registrado",13);
    memcpy(temporal.alias,"*************",13);
    memcpy(temporal.alias+13,"\0",1);
    memcpy(temporal.ip,ip+0,4);
    memcpy(temporal.mac,mac+0,6);
    InsertarFinal(l,temporal);
}
int BuscadorBase(lista *l,unsigned char *ip,unsigned char *mac)//en el buscador su respuesta indica si o encontro en la base y si esta permitido
{
    int indice;
    nodo *temporal;
    temporal=Primero(l);
    for(indice=0;indice<TamLista(l);indice++)
    {
        //(memcmp(TramaAEscuchar+28,MacIpCero,4)
        if(memcmp(temporal->objeto.mac,mac,6)==0 )
        {
            return True;
        }
    }
    return False;
}
void EstructuraTramaGratuitaDefensora(unsigned char *trama,unsigned char *MacInfractor,unsigned char *IpInfractor)
{
    //nota IpInfractor se supone que es la que estoy defendiendo 
    memcpy(trama+0,MacInfractor,6);//trama de broadcast
    memcpy(trama+6,MACTRON,6);//mi mac 
    memcpy(trama+12,arp,2);//protocolo de arp
    memcpy(trama+14,ether,2);//protocolo de ethernet
    memcpy(trama+16,proip,2);//tipo de protocolo ip
    trama[18]=6;//tamaño de la direccion mac
    trama[19]=4;//tamaño de la direccion ip
    memcpy(trama+20,arprequest,2);//arp request
    memcpy(trama+22,MACTRON,6);//mi mac
    memcpy(trama+28,IpInfractor,4);//mi ip
    memcpy(trama+32,MacInfractor,6);//mac destino
    memcpy(trama+38,IpInfractor,4);//ip destino se supone que es la que pide el infractor pero es la que defiendo
   
}
void EstructuraTramaGratuitaRespuesta(unsigned char *trama,unsigned char *IpInfractor)
{
    //nota IpInfractor se supone que es la que estoy defendiendo 
    memcpy(trama+0,MACbroadcast,6);//trama de broadcast
    memcpy(trama+6,MACTRON,6);//mi mac 
    memcpy(trama+12,arp,2);//protocolo de arp
    memcpy(trama+14,ether,2);//protocolo de ethernet
    memcpy(trama+16,proip,2);//tipo de protocolo ip
    trama[18]=6;//tamaño de la direccion mac
    trama[19]=4;//tamaño de la direccion ip
    memcpy(trama+20,arprequest,2);//arp request
    memcpy(trama+22,MACTRON,6);//mi mac
    memcpy(trama+28,IpInfractor,4);//mi ip
    memcpy(trama+32,MacIpCero,6);//mac destino
    memcpy(trama+38,IpInfractor,4);//ip destino se supone que es la que pide el infractor pero es la que defiendo
   
}
void EscaneaRed(int ds,int index,unsigned char *trama,unsigned char *tramarecivida,lista *l)
{
    int tam,indice,i,tiempo=0,j,total;
    struct sockaddr_ll nic;    
    struct timeval inicial,final;
    elementolista temporal;
    
    
    //struct timezone base;
    memset(&nic,0x00,sizeof(nic));
    nic.sll_family=AF_PACKET;
    nic.sll_protocol=htons(ETH_P_ARP);
    nic.sll_ifindex=index;
    
    puts("\n\n****************Escaneando****************");
    
    j=miip[2];
    total=j;
    total=total+2;
    for(j;j<total;j++)
    {
        printf(".");
        //printf("ciclo numero %d",j);
        for(indice=1;indice<255;indice++)
        {
            
            trama[40]=j;
            trama[41]=indice;
            memcpy(IPDestino,trama+38,4);//copia la trama porla que estamos preguntando 
   
            tam=sendto(ds,trama,42,0,(struct sockaddr *)&nic,sizeof(nic));//generar la trama por el socket
            if(tam ==-1)
            {
                perror("\nerror al enviar la trama ");
            }
            else//es por si si la envia 
            {
                //printf("\n exito fue enviada la trama de arp\nesperando respuesta *\n");
                
                //inicial.tv_usec=0;
                //final.tv_usec=0;
                gettimeofday(&inicial,NULL);
                while(1)
                {
                    //printf("\ncliclo interno %d\n",indice);
                    recvfrom(ds,tramarecivida,100,MSG_DONTWAIT,NULL,0);
                    if((memcmp(tramarecivida+28,IPDestino,4)==0)) // la trama que recibe es la direccion mac / se checa que corresponda la ip del quien la envia con la ip del quien pregunte
                    {
                        
                        memcpy(temporal.ip,IPDestino+0,4);
                        memcpy(temporal.mac,tramarecivida+6,6);
                        AgregaBaseExternos(l,temporal.ip,temporal.mac);
                        /*memcpy(temporal.alias,"No Registrado",13);
			memcpy(temporal.alias+13,"\0",1);
                        memcpy(temporal.ip,IPDestino+0,4);
                        memcpy(temporal.mac,tramarecivida+6,6);
                        InsertarFinal(l,temporal);*/

                        break;
                    }
                    gettimeofday(&final,NULL);
                    tiempo=final.tv_usec-inicial.tv_usec;
                    //printf("\nel tiempo es %d y no llego nada\n",tiempo);
                    if(tiempo>=1000 )
                    {
                        //printf("valor de tiempo %d\n",tiempo);
                        break;
                    }
                }
                //printf("fin ip %d.%d\n",j,indice);
                usleep(200000);
                tiempo=0;
            }
        }
        //printf(".");
        //usleep(2000000);
        
        //printf("\n*************************************************************************************************************\nciclo externo numero %d\n*************************************************************************************************************\n",j);
    }
} 
void ImprimeNodo(nodo *referencia,int cont)
{
    printf("\n****************************%d****************************************\n",cont);
    //printf("Nombre de Usuario: %s .",referencia->objeto.alias);
    printf("Nombre de Usuario: ");
    puts(referencia->objeto.alias);
    printf("La direccion IP: %d.%d.%d.%d",referencia->objeto.ip[0],referencia->objeto.ip[1],referencia->objeto.ip[2],referencia->objeto.ip[3]);
    printf("\nLa direccion Mac: %.2x:%.2x:%.2x:%.2x:%.2x:%.2x",referencia->objeto.mac[0],referencia->objeto.mac[1],referencia->objeto.mac[2],referencia->objeto.mac[3],referencia->objeto.mac[4],referencia->objeto.mac[5]);
    puts("\n********************************************************************\n");
}
void MostrarBase(lista *l)
{
    int indice;
    nodo *referencia;
    referencia=Primero(l);
    puts("\n****************Mostrando Base****************");
    for(indice=0;indice<TamLista(l);indice++)
    {
        ImprimeNodo(referencia,indice);
        referencia=Siguiente(l,referencia);
    }
}
/*******************************************************************************/
void estructuraTramaScanner( unsigned char *trama)//estructura la trama para arp
{
int i;
//unsigned short tipo=htons(ETH_P_ARP);
//unsigned short tipo2=htons(ARPHRD_ETHER);
//unsigned short tipo3=htons(ETH_P_IP);

	//printf("\nestructurando trama de arp\n");
	//puts("agregando mac broadcast");
		memcpy(trama+0,MACbroadcast,6);//trama de broadcast
	//puts("agregando mi mac");
		memcpy(trama+6,mimac,6);//mi mac 
	//puts("agregando protocolo arp");
		memcpy(trama+12,arp,2);//protocolo de arp
	//puts("agregando protocolo ethernet");
		memcpy(trama+14,ether,2);//protocolo de ethernet
	//puts("agregando el protocolo de ip");
		memcpy(trama+16,proip,2);//tipo de protocolo ip
	//puts("agregando el tamaño de direccion mac que usamos ");
		trama[18]=6;//tamaño de la direccion mac
	//puts("agregando el tamaño de la direccion ip que usamos ");
		trama[19]=4;//tamaño de la direccion ip
	//puts("agregando el mensaje a usar o indicando que es una pregunta de arp");
		memcpy(trama+20,arprequest,2);//arp request
	//puts("agregando mi mac dentro del mensaje de arp");
		memcpy(trama+22,mimac,6);//mi mac
	//puts("agregado mi ip dentro del mensaje de ip ");
		memcpy(trama+28,miip,4);//mi ip
	//puts("agregando mac destino dentro del mensaje ip");//en este caso es cero ya que eso lo agrega la trama que responde
		//memcpy(trama+32,0x00,6);//mac destino
	//puts("agregando ip destino ");//esta se agrega ya que estamos preguntnado por es ip en el mensaje 
		memcpy(trama+38,miip,2);//ip destino  
                //memcpy(trama+40,MACbroadcast,1);
	
/*
	puts("imprimiendo trama a enviar ");
	for(i = 0; i<42; i++)
	{
		printf("%d-", trama[i]);
	}
	puts("");
*/
	
}
int obtenerDatos(int ds)
{
	int i;
	int index;
	char nombre[10];
	struct ifreq interfaz;
        
	printf("Insertar nombre \n");
	scanf("%s",nombre);
	strcpy(interfaz.ifr_name,nombre);
	if(ioctl(ds,SIOCGIFINDEX,&interfaz)==-1)
		perror("\n error al obtener el indice");
	else
	{
		index=interfaz.ifr_ifindex;
		printf("\nEl indice es: 	%d\n",index);
		
		//Direccion MAC
		if(ioctl(ds,SIOCGIFHWADDR,&interfaz)==-1)//obtiene la direccion mac 
		{
			perror("\n Error");
		}
		else
		{
			memcpy(mimac, interfaz.ifr_dstaddr.sa_data,6);
			printf("La direccion mac es:\n");
			for(i = 0; i<6; i++)
			{
				printf("%.2x-", mimac[i]);
			}
		}
		//DireccionBroadcast
		if(ioctl(ds,SIOCGIFBRDADDR,&interfaz)==-1)//direccion de mi broadcast
		{
			perror("\nError");
		}
		else
		{	
		    memcpy(mibroad, interfaz.ifr_hwaddr.sa_data,8);		
		    printf("\nLa direccion de broadcast es: \n");
			for(i = 2; i<6; i++)
			{
				printf("%d.", mibroad[i]);
			}
		}
		//Mascara de subred
		if(ioctl(ds,SIOCGIFNETMASK,&interfaz)==-1)//mascara de subred
		{
			perror("\nError");
		}
		else
		{		
			memcpy(mimascara, interfaz.ifr_netmask.sa_data,6);
			printf("\nLa direccion de mascara de subred es: \n");
			
			for(i = 2; i<6; i++)
			{
				printf("%d.", mimascara[i]);
			}
			
		}
		//Direccion IP
		if(ioctl(ds,SIOCGIFADDR,&interfaz)==-1)//direccion ip
		{
			perror("\nError");
		}
		else
		{	
			memcpy(miip, interfaz.ifr_addr.sa_data+2,4);		
			printf("\nLa direccion IP es: \n");
			
			for(i = 0; i<4; i++)
			{				
				printf("%d-", miip[i]);
			}
		}
		
		printf("\n");
		
	}
	return index;	
	
}
main()
{
    lista UsuariosPermitidos;
    InicializarLista(&UsuariosPermitidos);
    elementolista temporal;
    
    int packet_socket, indice;
    packet_socket=socket(AF_PACKET, SOCK_RAW, htons(ETH_P_ARP));
    
    
    if(packet_socket==-1)
        perror("\n Error al abrir el socket\n");
    else
    {

         printf("\nExito al abrir el socket\n");
         
         indice=obtenerDatos(packet_socket);
         //Cargar A la base de datos informacion tanto del servidor como defensor 
         memcpy(temporal.ip,miip,4);//datos del servidor
         memcpy(temporal.mac,mimac,6);
         memcpy(temporal.alias,"Servidor ARP",12);
         memcpy(temporal.alias+12,"\0",1);
         AgregaBasePaquete(&UsuariosPermitidos,temporal);
         memcpy(temporal.ip,MacIpCero,4);//DAtos TRON
         memcpy(temporal.mac,MACTRON,6);
         memcpy(temporal.alias,"Servidor TRON",13);
         memcpy(temporal.alias+13,"\0",1);
         AgregaBasePaquete(&UsuariosPermitidos,temporal);
         //AgregaBase(&UsuariosPermitidos,temporal.ip,temporal.mac);//agrega los datos del servidor a la base para poder ser usados 
                 
         estructuraTramaScanner(tramaEnv);
         EscaneaRed(packet_socket,indice,tramaEnv,recivTrama,&UsuariosPermitidos);
         MostrarBase(&UsuariosPermitidos);
         //TRON(packet_socket,indice,&UsuariosPermitidos);
         printf("\nFin del programa\n");
         close(packet_socket);
        
    }
}
//
